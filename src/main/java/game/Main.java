package game;

public class Main {

    public static void main(String[] args) {
        Game game = new Game(
                new Hero(), new Hall(
                new Door[]{
                        new Door(new Artifact()),
                        new Door(new Artifact()),
                        new Door(new Artifact()),
                        new Door(new Artifact()),
                        new Door(new Artifact()),
                        new Door(new Artifact()),
                        new Door(new Artifact()),
                        new Door(new Monster()),
                        new Door(new Monster()),
                        new Door(new Monster()),
                }
        )
        );
        game.start();
    }
}
