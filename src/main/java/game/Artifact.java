package game;

public class Artifact implements Affectable{

    private int strength;

    public Artifact() {
        this.strength = (int)(10 + (Math.random() * (80 - 10)));
    }

    public Artifact(int strength) {
        this.strength = strength;
    }


    @Override
    public void affect(Hero hero) {
        hero.addStrength(this.strength);
    }

    @Override
    public int getStrengthOfAffect() {
        return this.strength;
    }
}
