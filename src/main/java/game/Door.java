package game;

public class Door {

    private Affectable affectable;

    public Door(Affectable affectable) {
        this.affectable = affectable;
    }

    void open(Hero hero) {
        affectable.affect(hero);
    }

    public Affectable getAffectable() {
        return affectable;
    }

    @Override
    public String toString() {
        return "" + affectable.getClass().getSimpleName() +
                " : " + affectable.getStrengthOfAffect() +
                '\n';
    }

    public int getAffectableStrength() {
        return affectable.getStrengthOfAffect();
    }
}
