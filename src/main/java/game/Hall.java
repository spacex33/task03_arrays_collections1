package game;

public class Hall {

    private Door[] doors;

    public Hall(Door[] doors) {
        this.doors = doors;
    }

    public Door[] getDoors() {
        return doors;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Door door:doors) {
            sb.append(door.toString());
        }
        return sb.toString();
    }
}
