package game;

import array.Array;

import java.util.Arrays;

public class Game {

    private Hero hero;
    private Hall hall;

    public Game(Hero hero, Hall hall) {
        this.hero = hero;
        this.hall = hall;
    }

    public void start() {

        System.out.println(countDeathDoors(0,0));

        for (int i : safeSequenceOfDoors(this.hall.getDoors())) {
            System.out.print(i + " ");
        }

        System.out.println();

        printDoorsInfo();
    }


    /**
     * The method returns the door numbers sequence in the order
     * in which they should be opened by the hero to stay alive.
     * <p>
     * We can't find the safe sequence of choices, when we encounter with big amount of monsters
     * If the sum of strength of monsters exceeds the sum of strength of the artifacts and the hero,
     * there is no safe sequence available
     *
     * @param doors an array of doors that we have to open in the right order.
     * @return door numbers sequence.
     * @throws RuinedGameBalanceException method can't do his job if the game balance is violated.
     */
    public int[] safeSequenceOfDoors(Door[] doors) {

        // if the monsters behind the door are too strong,
        // throw the corresponding exception
        if (!isGameBalanced(doors)) {
            throw new RuinedGameBalanceException(
                    "The generated monsters behind the door are too strong. No winning script");
        }

        int heroHealth;

        // array for sequences of indexes that we use
        int[] usedIndex = new int[doors.length];

        // infinite loop, that works until we find a solution
        while (true) {

            // each iteration we fill our array by value,
            // which is different from the indices we operate
            Arrays.fill(usedIndex, -1);

            // each iteration we update health of the hero
            // by the standard value that we take from his class
            heroHealth = hero.getStrength();

            for (int i = 0; i < doors.length; i++) {

                // we trying at the first time to get random index from array of doors
                int index = getRandomIndex(doors);

                // if not unique, try again while contains become not true
                while (Array.contains(usedIndex, index)) {

                    // continue the attempts until we find the index,
                    // which is not yet in the 'usedIndex' array
                    index = getRandomIndex(doors);

                }
                // ok, while - false, that means we got unique index for this iteration,
                // lets save it in array!
                usedIndex[i] = index;

                // now we need to figure out what effect we receive behind the door
                if (doors[index].getAffectable() instanceof Monster) {
                    heroHealth -= doors[index].getAffectableStrength();
                }

                if (doors[index].getAffectable() instanceof Artifact) {
                    heroHealth += doors[index].getAffectableStrength();
                }

                // if the hero's health went into a minus, then we went the wrong way
                if (heroHealth < 0) {
                    // finish the current loop
                    break;
                }
            }

            // if the loop above is over and the health of the hero a positive value,
            // so we went in the right way
            if (heroHealth > 0) {

                // than we can end the infinite loop
                break;
            }

        }
        return usedIndex;
    }


    private boolean isGameBalanced(Door[] doors) {
        int amountOfMonsterStrength = 0;
        int amountOfArtifactStrength = 0;

        for (Door door : doors) {
            if (door.getAffectable() instanceof Monster) {
                amountOfMonsterStrength = door.getAffectableStrength();
            }

            if (door.getAffectable() instanceof Artifact) {
                amountOfArtifactStrength = door.getAffectableStrength();
            }
        }
        return amountOfArtifactStrength > amountOfMonsterStrength;
    }

    private int getRandomIndex(Door[] arr) {
        return (int) (0 + (Math.random() * (arr.length)));
    }

    /**
     * Method recursively calculates how many doors for the hero can be deadly.
     *
     * @param deathDoorCount represents doors with dead.
     * @param doorIndex      represents index of door
     * @return calling this method
     */
    public int countDeathDoors(int doorIndex, int deathDoorCount) {
        if (doorIndex == this.hall.getDoors().length) {
            return deathDoorCount;
        }

        if (this.hall.getDoors()[doorIndex].getAffectable() instanceof Monster &&
                this.hall.getDoors()[doorIndex].getAffectableStrength() > this.hero.getStrength()) {
            deathDoorCount++;
        }

        return countDeathDoors(doorIndex + 1, deathDoorCount);
    }

    private void printDoorsInfo() {
        System.out.println(hall.toString());
    }
}
