package game;

public class Monster implements Affectable{

    private int strength;

    public Monster() {
        this.strength = (int)(5 + (Math.random() * (100 - 5)));
    }

    public Monster(int strength) {
        this.strength = strength;
    }

    @Override
    public void affect(Hero hero) {
        hero.removeStrength(this.strength);
    }

    @Override
    public int getStrengthOfAffect() {
        return this.strength;
    }
}
