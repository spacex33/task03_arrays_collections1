package game;

public class Hero {

    private int strength = 25;

    public Hero() {
    }

    public void addStrength(int num) {
        strength += num;
    }

    public void removeStrength(int num) {
        strength -= num;
    }

    public int getStrength() {
        return strength;
    }
}
