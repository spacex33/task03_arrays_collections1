package game;

/**
 * An exception is thrown when we are faced with a violated game balance,
 * when a player does not have the opportunity to win due to the generated game elements.
 */
public class RuinedGameBalanceException extends RuntimeException {

    public RuinedGameBalanceException(String message) {
        super(message);
    }
}
