package game;

public interface Affectable {

    void affect(Hero hero);

    int getStrengthOfAffect();
}
