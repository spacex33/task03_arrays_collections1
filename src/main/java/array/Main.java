package array;

public class Main {

    public static void main(String[] args) {

        int[] arr = Array.getArrayOfElementsPresentedInOneOf(new int[]{1,2,3,4,5,6,6}, new int[]{9,8,7,6,5,4,6,6,6});
        System.out.println("RESULT: ");
        for (int i:arr) {
            System.out.print(i);
        }
        System.out.println();

        int[] arr2 = Array.removeDuplicateNumbers(new int[]{1,2,2,3,3,3,4,5,6,6,6});
        System.out.println("RESULT: ");
        for (int i:arr2) {
            System.out.print(i);
        }
        System.out.println();

        int[] arr3 = Array.removeDuplicateSeries(new int[]{1,2,2,8,3,3,3,4,5,6,6,6,6,2});
        System.out.println("RESULT: ");
        for (int i:arr3) {
            System.out.print(i);
        }
    }
}
