package container;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();

        StringArrayContainer sac = new StringArrayContainer();
        for (int i = 0; i < 30000; i++) {
            sac.add("Hello!");
        }
        long totalTime = System.currentTimeMillis() - startTime;
        System.out.println(totalTime);

        long startTime2 = System.currentTimeMillis();

        List<String> arrayList = new ArrayList<>();
        for (int i = 0; i < 30000; i++) {
            arrayList.add("Hello!");
        }
        long totalTime2 = System.currentTimeMillis() - startTime2;
        System.out.println(totalTime2);

        // 679 / 5
    }
}
