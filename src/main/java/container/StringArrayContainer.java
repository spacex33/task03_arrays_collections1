package container;

public class StringArrayContainer {

    private static final int DEFAULT_CAPACITY = 10;

    private String[] elementData;

    private int size = 0;

    public StringArrayContainer(int capacity) {
        elementData = new String[capacity];
    }

    public StringArrayContainer() {
        this(DEFAULT_CAPACITY);
    }

    public void add(String element) {

        if (this.size == elementData.length) {
            this.elementData = updateArray(this.elementData, this.size + 1);
        }

        this.elementData[this.size++] = element;
    }

    public String get(int index) {
        return this.elementData[index];
    }

    private static String[] updateArray(String[] src, int size) {
        String[] dest = new String[size];

        if (size > src.length) {
            System.arraycopy(src, 0, dest, 0, src.length);
        } else {
            System.arraycopy(src, 0, dest, 0, size);
        }

        return dest;
    }
}
