package droids;

public class Main {

    public static void main(String[] args) {
        Droid droid1 = new Droid("S54SD");
        Droid droid2 = new Droid("G4LM4");
        Droid droid3 = new Droid("A454S");

        Ship<Droid> droids = new Ship<>();
        droids.add(droid1);
        droids.add(droid2);
        droids.add(droid3);

        DroidPriorityQueue<Droid> droidPriorityQueue = new DroidPriorityQueue<>();
        droidPriorityQueue.addAll(droids);

        for (Droid droid:droidPriorityQueue) {
            System.out.println(droid);
        }

    }
}
