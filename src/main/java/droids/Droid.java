package droids;

public class Droid implements Comparable{

    private String name;

    public Droid(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Object o) {
        return this.name.compareToIgnoreCase(((Droid)o).getName());
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name=" + name +
                '}';
    }
}
