package comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Country> countries = new ArrayList<>();
        countries.add(new Country("USA", "Washington"));
        countries.add(new Country("Turkey", "Ankara"));
        countries.add(new Country("Albania", "Tirana"));
        System.out.println(countries);

        Collections.sort(countries);
        System.out.println(countries);

        countries.sort(new CapitalComparator());
        System.out.println(countries);

        int index = Collections.binarySearch(
                countries,
                new Country("Turkey", "Ankara"),
                new CapitalComparator());

        System.out.println(countries.get(index));
    }

    public static class CapitalComparator implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            return ((Country)o1).getCapital().compareToIgnoreCase(((Country)o2).getCapital());
        }
    }
}
